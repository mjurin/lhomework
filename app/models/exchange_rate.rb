class ExchangeRate < ActiveRecord::Base
  DECIMAL_REGEX = /\A[-+]?[0-9]*\.?[0-9]+\Z/

  validates :release_date, uniqueness: true,
                           presence: true
  validates :rate,  presence:   true,
                    format:     { with: DECIMAL_REGEX }

  scope :for_date, -> date { where('release_date <= ?', date) }

  def self.return_rate_for_date date
    for_date(date).last.rate if for_date(date).last
  end

  def self.last_release_date
    last_exchange_rate = ExchangeRate.order('release_date DESC').first
    return last_exchange_rate.release_date if last_exchange_rate
    Date.new(1970, 1, 1)
  end
end
