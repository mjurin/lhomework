namespace :db do
  task :ecb_fetch_exchange_rates => :environment do
    csv_file = ExchangeRatesFromEcb.get_csv
    ExchangeRatesFromEcb.store_exchange_rates_from_csv csv_file
  end
end
