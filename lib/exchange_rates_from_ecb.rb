class ExchangeRatesFromEcb
  BASE_URL = 'http://sdw.ecb.europa.eu'
  PATH = '/export.do?type=&trans=N&node=2018794&CURRENCY=USD&FREQ=D&start=01­01­2012&q=&submitOptions.y=6&submitOptions.x=51&sfl1=4&end=&SERIES_KEY=120.EXR.D.USD.EUR.SP00.A&sfl3=4&DATASET=0&exportType=csv'
  FULL_URL = BASE_URL + PATH

  def self.get_csv
    require 'open-uri'
    require 'csv'
    csv = nil
    begin
      file_content = open(URI.parse(URI.escape(FULL_URL)))
      csv_text = File.read(file_content)
      csv = CSV.parse(csv_text, headers: false)
    rescue OpenURI::HTTPError
    end
    csv
  end

  def self.store_exchange_rates_from_csv(csv,
    release_date = ExchangeRate.last_release_date)

    csv.each_with_index do |row, index|
      if row[1] && row[1] =~ ExchangeRate::DECIMAL_REGEX
        if release_date < row[0].to_date
          ExchangeRate.create release_date: row[0], rate: row[1]
        else
          break
        end
      end
    end
  end

end
