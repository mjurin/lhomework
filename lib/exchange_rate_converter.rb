class ExchangeRateConverter
  DATE_REGEX = /(\d{4}-\d{2}-\d{2})/

  def self.convert value, date
    return "#{date} date is not valid" unless date =~ DATE_REGEX
    rate = ExchangeRate.return_rate_for_date date
    return (value / rate).round(4) if rate
    'There are no rate before ' + date.to_s
  end
end
