class CreateExchangeRates < ActiveRecord::Migration
  def change
    create_table :exchange_rates do |t|
      t.date :release_date
      t.decimal :rate, precision: 6, scale: 4
      t.timestamps null: false
    end

    add_index :exchange_rates, [:release_date], unique: true
  end
end
