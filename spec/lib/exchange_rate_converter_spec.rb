require 'rails_helper'

describe ExchangeRateConverter do

  let(:create_defult_exchange_rate) {
    create :exchange_rate, release_date: Date.new(2016, 1, 4), rate: 1.0898
  }
  describe 'Exchange rate converter convert' do
    before do
      create_defult_exchange_rate
    end

    it 'expects to convert 1 dollar to 0.9176 euros for given date and rate' do
      converted = ExchangeRateConverter.convert(1, '2016-01-04')
      expect(converted).to eq (1/1.0898).round(4)
    end

    it 'expects to return error if there is no conversion for or before date' do
      test_date = '2015-01-04'
      expect(ExchangeRateConverter.convert(1, test_date)
        ).to eq 'There are no rate before '+ test_date.to_s
    end

    it 'expects to take closes exchange rate' do
      expect(ExchangeRateConverter.convert(1, '2016-01-07')
        ).to eq (1/1.0898).round(4)
    end

    it 'expects to return error if date not valid' do
      expect(ExchangeRateConverter.convert(1, '2016-01-')
        ).to eq '2016-01- date is not valid'
    end
  end
end
