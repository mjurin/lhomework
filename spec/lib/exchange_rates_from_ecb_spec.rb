require 'rails_helper'
require 'csv'

describe ExchangeRatesFromEcb do

  let(:exchange_rates_csv_file) {
    file = IO.read(Rails.root.join("spec", "fixtures", "exchange_rates.csv"))
    csv = CSV.parse(file, headers: true)
    csv
  }

  describe 'Store exchange rates to database' do
    it 'Expect to change the number of rates by the number of exchange rates in
        the file' do
      expect{
        ExchangeRatesFromEcb.store_exchange_rates_from_csv exchange_rates_csv_file
      }.to change { ExchangeRate.count }.by 6
    end

    it 'Expect not to store exchange rates, because they are older than the last one' do
      #last rate from the csv file
      create :exchange_rate, release_date: Date.new(2016, 2, 15), rate: 1.1180
      expect{
        ExchangeRatesFromEcb.store_exchange_rates_from_csv exchange_rates_csv_file
      }.to change { ExchangeRate.count }.by 0
    end
  end

end
