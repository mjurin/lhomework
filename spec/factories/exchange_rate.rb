FactoryGirl.define do
  factory :exchange_rate do
    release_date Date.new(2016, 1, 4)
    rate 1.0898
  end
end
